"use strict";
// function createNewUser() {
//     return {
//         firstName: prompt("Enter first name"),
//         lastName: prompt("Enter last name"),
//         getLogin() {
//             return (this.firstName[0] + this.lastName).toLowerCase();
//         }
//     };
// }

function createNewUser() {
    let firstName = "";
    let lastName = "";
    do {
        firstName = prompt("Enter first name");
        lastName = prompt("Enter last name");
    } while (firstName.trim() === "" || firstName === null &&
    lastName.trim() === "" || lastName === null);

    const newUser = {};
    Object.defineProperties(newUser, {
        firstName: {
            value: firstName,
            configurable: true
        },

        lastName: {
            value: lastName,
            configurable: true
        },

        getLogin: {
            get: function () {
                return (this.firstName[0] + this.lastName).toLowerCase()
            }
        },

        setFirstName: {
            set: function(newValue) {
                Object.defineProperty(this, "firstName", {
                    value: newValue
                });
            },
            enumerable: true
        },

        setLastName: {
            set: function (newValue) {
                Object.defineProperty(this, "lastName", {
                    value: newValue
                });

            },
            enumerable: true
        }
    });

    return newUser;
}


let user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin);

console.log(`User info:\nName: \t\t${user1.firstName}\nLast name:\t${user1.lastName}\nUser login:\t${user1.getLogin}`);

user1.setFirstName = prompt("Enter new name");
user1.setLastName = prompt("Enter new last name");

console.log(`User info:\nName: \t\t${user1.firstName}\nLast name:\t${user1.lastName}\nUser login:\t${user1.getLogin}`);


