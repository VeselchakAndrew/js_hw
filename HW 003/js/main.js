let userNumber1 = 0;
let userNumber2 = 0;
let operation = "";

do {
    userNumber1 = prompt("Enter first number", userNumber1);
    userNumber2 = prompt("Enter second number", userNumber2);
} while (userNumber1 === null || isNaN(Number(userNumber1)) || userNumber1.trim() === ""
&& userNumber2 === null || isNaN(Number(userNumber2)) || userNumber2.trim() === "");

do {
    operation = prompt("Choose the operation (+, -, *, /)");
} while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/");

function calculator(num1, num2, operation) {
    switch (operation) {
        case "+":
            return +num1 + +num2;

        case "-":
            return +num1 - +num2;

        case "*":
            return +num1 * +num2;

        case "/":
            return +num1 / +num2;


    }
}

console.log(`${userNumber1} ${operation} ${userNumber2} = ${calculator(userNumber1, userNumber2, operation) }`);

