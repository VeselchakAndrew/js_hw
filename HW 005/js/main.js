function createNewUser() {
    let firstName = "";
    let lastName = "";
    let birthday = "";
    let re = /\d{2}\.\d{2}\.\d{4}/;
    do {
        firstName = prompt("Enter first name");
        lastName = prompt("Enter last name");
    } while (firstName.trim() === "" || firstName === null &&
    lastName.trim() === "" || lastName === null);

    do {
        birthday = prompt("Enter your birthday (DD.MM.YYYY)")
    } while (!(re.test(birthday)));



    const newUser = {};
    Object.defineProperties(newUser, {
        firstName: {
            value: firstName,
            configurable: true
        },

        lastName: {
            value: lastName,
            configurable: true
        },

        birthday: {
            value: birthday,
            configurable: true
        },

        getLogin: {
            get: function () {
                return (this.firstName[0] + this.lastName).toLowerCase()
            }
        },
/* если вычислять возраст через отнимание двух значений времени и перевода из мс в года,
то за счет высокосных годов набегает погрешность в несколько дней (при возрасте в 40 лет погрешность в 11 дней)
Math.floor((date - birth) / (365 * 24 * 60 * 60 * 1000))
*/
        getAge: {
            get: function () {
                let date = new Date();
                let dateOfBirth = birthday.split(".");
                let birth = new Date(dateOfBirth[2], (dateOfBirth[1] - 1), dateOfBirth[0]);
                let age = date.getFullYear() - birth.getFullYear();
                if ((date.getMonth() - birth.getMonth()) < 0) {
                    age -= 1;
                } else if ((date.getMonth() - birth.getMonth()) === 0) {
                    if ((date.getDate() - birth.getDate()) < 0) {
                        age -= 1;
                    }
                }
                return age;
            }
        },

        getPassword: {
            get: function () {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
            }
        },
        getUserInfo: {
            get: function () {
                return (`User info:\nUser name:\t\t${this.firstName}\nUser last name:\t${this.lastName}\n` +
                `User age:\t\t${this.getAge}\nUser login:\t\t${this.getLogin}\nUser password:\t${this.getPassword}`);
            }
        },

        setFirstName: {
            set: function (newValue) {
                Object.defineProperty(this, "firstName", {
                    value: newValue
                });
            },
            enumerable: true
        },

        setLastName: {
            set: function (newValue) {
                Object.defineProperty(this, "lastName", {
                    value: newValue
                });

            },
            enumerable: true
        }
    });

    return newUser;
}

let user1 = createNewUser();
console.log(user1.getAge);
console.log(user1.getPassword);
console.log(user1.getUserInfo);