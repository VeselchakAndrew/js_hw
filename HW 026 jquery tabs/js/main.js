jQuery(function () {
    const tabs = $(".tabs > li");
    const firstTab = $(".tabs > li:first-child");
    const tabsContent = $(".tabs-content > li");
    const firstTabContent = $(".tabs-content > li:first-child");

    tabs.addClass("tabs-title");
    firstTab.addClass("button--active");
    tabs.attr("data-for-tab", function (i) {
        return i++
    });

    tabsContent.addClass("tab");
    firstTabContent.addClass("tabs--active");
    tabsContent.attr("data-tab", function (i) {
        return i++
    });

    $(".tabs-title").click(function () {
        let num = $(this).attr("data-for-tab");
        let contentTab = $(`.tab[data-tab="${num}"]`);

        tabs.removeClass("button--active");
        $(this).addClass("button--active");

        tabsContent.removeClass("tabs--active");
        contentTab.addClass("tabs--active");
    })
});