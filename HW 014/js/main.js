jQuery(function () {
    let topButton = $(".to-top");
    topButton.fadeOut(0);

    let toggleButton = $(".toggle");
    let toggleDiv = $(".posts_img");


// Smooth scroll
    $(`.onpage-nav__item > a[href^="#"]`).click(function () {
        let target = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(target).offset().top
        }, 500);
    });

// To Top button
//     let windowHeight = $(window).height();
    function setHeight() {
        return $(window).height()
    }

    setHeight();
    $(window).resize(setHeight);

    $(window).scroll(function () {
        let height = $(document).scrollTop();
        if (height > setHeight()) {
            topButton.fadeIn(500);
        } else {
            topButton.fadeOut(500);
        }

        topButton.click(function () {
            $("html, body").stop().animate({
                scrollTop: "0"
            }, 500);
        })
    });

// Slide toggle block
    toggleButton.click(function () {
        toggleDiv.slideToggle(300);
    })


});