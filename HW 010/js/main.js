const showPass = document.querySelectorAll(".icon-password");
const passSubmit = document.getElementById("password_submit");

function togglePass(button) {
    let passForm = button.closest(".input-wrapper").querySelector("input");

    if (passForm.type === "password") {
        passForm.type = "text"
    }
    else {
        passForm.type = "password"
    }
}

function changeIcon(button) {
    if (button.classList.contains("fa-eye-slash")) {
        button.classList.remove("fa-eye-slash");
        button.classList.add("fa-eye");
    }
    else {
        button.classList.remove("fa-eye");
        button.classList.add("fa-eye-slash");
    }
}

function checkPass() {
    const pass = document.querySelector("#pass").value;
    const checkPass = document.querySelector("#pass_check").value;
    const errorSpan = document.querySelector(".error");

    if (pass === checkPass) {
        if (errorSpan) {
            errorSpan.remove();
        }
        setTimeout(wellcomeMessage, 0);
    }
    else {
        if (!errorSpan) {
            const button = document.querySelector(".btn");
            const error = `<span class="error">Нужно ввести одинаковые значения</span>`;
            button.insertAdjacentHTML("beforebegin", error);
        }
    }

}

function preventDef(event) {
    event.preventDefault();
}

function wellcomeMessage() {
    alert("You are welcome");
}

showPass.forEach(button => {
    button.addEventListener("click", () => {
        togglePass(button);
        changeIcon(button);
    })
});

passSubmit.addEventListener("click", (event) => {
    checkPass();
    preventDef(event)
});