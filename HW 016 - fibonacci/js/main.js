function fibonacci(f0, f1, n) {
    if (n === 0) {
        return f0;
    }
    else if (n === 1) {
        return f1;
    }
    else {
        if (n > 0) {
            return fibonacci(f1, f0 + f1, n - 1)
        }
        else {
            return fibonacci(f1, f0 - f1, n + 1)
        }
    }
}
let f0 = 0;
let f1 = 1;
let n = 0;
do {
    f0 = prompt("Enter fibonacci first number", f0);
    f1 = prompt("Enter fibonacci second number", f1);
    n = prompt("Enter fibonacci index", n);
} while (n === null || isNaN(Number(n)) || n.trim() === "" &&
        f0 === null || isNaN(Number(f0)) || f0.trim() === "" &&
        f1 === null || isNaN(Number(f1)) || f1.trim() === "");

// Приведение переменных к типу number

f0 = +f0;
f1 = +f1;
n = +n;

console.log(fibonacci(f0, f1, n));