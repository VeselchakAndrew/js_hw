function factorial(n) {
    if (n === 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

let index = 0;
do {
    index = prompt("Enter positive number");
    } while (index === null || isNaN(Number(index)) || index.trim() === "" || index < 0 );

console.log(`Факториал от ${index} =  ${factorial(index)}`);