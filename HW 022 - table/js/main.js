const wrap = document.querySelector(".wrapper");

const table = document.createElement("table");
table.classList.add("table");

let rows = 30;
let columns = 30;

for (let i = 0; i < rows; i++) {
    const tr = document.createElement("tr");
    for (j = 0; j < columns; j++) {
        const td = document.createElement("td");
        tr.append(td);
    }
    table.append(tr);
}

wrap.append(table);

table.addEventListener("click", (event) => {

    if (event.target.closest("tr")) {
        event.target.classList.toggle("highlight")
    }
});


document.body.addEventListener("click", (event) => {
    const targetEl = event.target;

    if (targetEl === document.body) {
        table.classList.toggle("revert")
    }

    console.log(targetEl);
});

