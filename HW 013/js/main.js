const btn = document.querySelector("#change_theme");
const theme = document.querySelector("#theme");
const cssFile = document.querySelector("#css_theme");
const preferedTheme = localStorage.getItem("css_theme");

console.log(preferedTheme);

function cssTheme() {
    if (preferedTheme === "default" || undefined) {
        cssFile.href = "css/style_default.css";
        theme.selectedIndex = "0";
    }

    else {
        cssFile.href = "css/style_dark.css";
        theme.selectedIndex = "1";
    }
}

document.addEventListener("DOMContentLoaded", cssTheme);

btn.addEventListener("click", () => {
    localStorage.setItem("css_theme", theme.value);
    location.reload()
});