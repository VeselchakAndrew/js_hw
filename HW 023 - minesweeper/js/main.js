const board = document.querySelector(".board");
const display = document.querySelector(".info");
const startButton = document.querySelector(".startButton");

let boardSize = 8;
let numberOfBombs = 10;
let matrix = [];
let bombsCoordinates = [];
let isPlaying = false;
let numberOfFlagsWithBombs = 0;
let clickCount = 0;

// load board
function loadBoard() {
    let sizePercent = 100 / boardSize;
    let allCoordinates = [];
    numberOfFlagsWithBombs = 0;
    clickCount = 0;

    board.innerHTML = "";

    display.innerText = numberOfBombs;

    matrix = new Array(boardSize);
    for (let y = 0; y < boardSize; y++) {
        let rowDiv = document.createElement("div");
        rowDiv.classList.add("rowDiv");

        matrix[y] = new Array(boardSize);
        for (let x = 0; x < boardSize; x++) {
            let cell = document.createElement("div");
            cell.classList.add("cell");
            cell.style.height = sizePercent + "%";

            rowDiv.appendChild(cell);

            matrix[y][x] = cell;
            cell.x = x;
            cell.y = y;

            allCoordinates.push(cell);

            cell.onclick = (e) => {
                leftClickAction(e);
            };
            cell.oncontextmenu = (e) => {
                rightClickAction(e);
            };
        }

        board.appendChild(rowDiv);
    }

    generateBombs(allCoordinates);
}

function generateBombs(allCoordinates) {
    bombsCoordinates = [];

    let count = 0;
    while (count++ < numberOfBombs) {
        let idx = Math.floor(Math.random() * allCoordinates.length);

        let coordinate = allCoordinates[idx];
        matrix[coordinate.y][coordinate.x].bomb = true;

        allCoordinates.splice(idx, 1);

        bombsCoordinates.push(coordinate);
    }
}

function showBombs() {
    for (let idx in bombsCoordinates) {
        let x = bombsCoordinates[idx].x;
        let y = bombsCoordinates[idx].y;

        matrix[y][x].classList.add("show_hidden_bomb");
        matrix[y][x].innerText = "*";
    }
}

function leftClickAction(event) {
    if (!isPlaying) return;

    let cell = getParent(event.target, "DIV");

    if (cell.markedAsFlag) {
        let isHumanClick = event.isTrusted;

        if (isHumanClick) {
            return;
        } else {
            if (!cell.bomb) {
                hideFlag(cell);
            }
        }
    }

    hitPosition(cell, arguments[1]);
    clickCount++;

    if (clickCount === 1) {
        startButton.classList.remove("hide");
    }
}

function rightClickAction(event) {
    event.preventDefault();

    if (!isPlaying) return;

    let target = event.target;

    if (target.nodeName === "DIV" && target.markedAsFlag == null) {
        showFlag(target);
    } else {
        hideFlag(target);
    }
}

function showFlag(element) {
    if (display.innerText !== "0") {
        element.innerHTML = `<span class="flagtriangle"></span>`;
        element.markedAsFlag = true;

        display.innerText = parseInt(display.innerText) - 1;

        if (element.bomb) {
            numberOfFlagsWithBombs++;
        }

        if (numberOfFlagsWithBombs === numberOfBombs) {
            isWinner();
        }
    }
}

function hideFlag(element) {
    element = getParent(element, "DIV");

    element.innerHTML = "";
    element.markedAsFlag = null;

    display.innerText = parseInt(display.innerText) + 1;

    if (element.bomb) {
        numberOfFlagsWithBombs--;
    }
}

function getParent(element, nodeName) {
    while (element.nodeName !== nodeName) element = element.parentNode;

    return element;
}

function hitPosition(cell) {
    cell.onclick = null;
    cell.oncontextmenu = (e) => {
        e.preventDefault();
    };

    if (cell.bomb) {
        gameOver(cell);
    } else {
        let neighborBombs = howManyNeighborBombs(cell.x, cell.y);

        cell.neighborBombs = neighborBombs;

        if (neighborBombs !== 0) {
            cell.innerText = neighborBombs;

            const colors = [
                "red",
                "teal",
                "brown",
                "rebeccapurple",
                "purple",
                "darkgreen",
                "green",
                "navy",
            ];

            cell.style.color = colors[colors.length - neighborBombs];
        } else {
            let dontExplode = arguments[1];

            if (!dontExplode) {
                explodeInterative(cell.x, cell.y);
            }
        }

        cell.classList.add("hidden_cell");
    }
}

function gameOver(cell) {
    isPlaying = false;
    showBombs();
    cell.classList.add("bomb");

    display.innerText = "game over";
    display.classList.add("gameover");
}

function howManyNeighborBombs(x, y) {
    let count = 0;

    let iy = y === 0 ? y : y - 1;
    for (; iy <= y + 1 && iy < matrix.length; iy++) {
        let ix = x === 0 ? x : x - 1;
        for (; ix <= x + 1 && ix < matrix[iy].length; ix++) {
            if (matrix[iy][ix].bomb) count++;
        }
    }

    return count;
}

function explodeInterative(x, y) {
    let scheduledPositions = [];

    let pos = {};
    pos.x = x;
    pos.y = y;

    scheduledPositions = scheduledPositions.concat(gatherNeighborPositions(pos));

    while (scheduledPositions.length > 0) {
        let currentPosition = scheduledPositions.pop();
        let ignoreExplode = true;

        let event = {};
        event.target = currentPosition;

        leftClickAction(event, ignoreExplode);

        if (currentPosition.neighborBombs === 0) {
            let newPositions = gatherNeighborPositions(currentPosition);

            scheduledPositions = scheduledPositions.concat(newPositions);
        }
    }
}

function gatherNeighborPositions(pos) {
    let result = [];

    let y = pos.y;
    let x = pos.x;

    let iy = y === 0 ? y : y - 1;
    for (; iy <= y + 1 && iy < matrix.length; iy++) {
        let ix = x === 0 ? x : x - 1;
        for (; ix <= x + 1 && ix < matrix[iy].length; ix++) {
            if (matrix[iy][ix].onclick == null) continue;

            result.push(matrix[iy][ix]);
        }
    }

    return result;
}

function isWinner() {
    isPlaying = false;
    display.classList.add("won");
    display.innerText = "YOU WON!!!";
}

startButton.onclick = () => {
    loadBoard();
    startButton.classList.add("hide");
    display.classList.remove("won");
    display.classList.remove("gameover");
    isPlaying = true;
};

startButton.click();
