"use strict";

const display = document.querySelector(".display");
const keys = document.querySelector(".keys");
const buttons = keys.querySelectorAll("input");
const output = display.querySelector("input");
const operators = ["=", "+", "*", "/", "C", "m-", "m+", "mrc", "Enter"];
let memory = "0";
let result = "";
let condenser = [];
let clickCount = 0;
let firstValue = 0;

function insert(num) {
    output.value = output.value + num;
}

function reset() {
    output.value = "";
}

function calculator(arr) {

    if (arr[1] === "/" && arr[2] === "") {
        return "Division by zero"
    }

    switch (arr[1]) {
        case "-":
            return +arr[0] - +arr[2];

        case "+":
            return +arr[0] + +arr[2];

        case "*":
            return +arr[0] * +arr[2];

        case "/":
            return +arr[0] / +arr[2];

        default:
            break;
    }
}

function addZero() {
    output.value = "0";
}

function main(keyValue) {

    if (!operators.includes(keyValue)) {

        if (output.value === "0" || output.value === firstValue || output.value === result) {
            reset();
        }
        insert(keyValue);
    } else if (keyValue === "C") {
        output.value = "0";
        condenser = [];
    } else if (keyValue === "=" || keyValue === "Enter") {
        condenser.push(output.value);

        if (condenser.length > 2 && operators.includes(condenser[1])) {
            output.value = calculator(condenser);
            result = output.value;
        }
        condenser = [];
    } else if (keyValue === "m-" || keyValue === "m+") {
        memory = output.value;
        display.insertAdjacentHTML("afterbegin", `<span class="memory">M</span>`)
    } else if (keyValue === "mrc") {

        if (clickCount === 0) {
            reset();
            output.value = memory;
            clickCount++;
        } else {
            output.value = "0";
            memory = "0";
            clickCount--;
            document.querySelector(".memory").remove();
        }
    } else {
        firstValue = output.value;
        condenser.push(firstValue);
        condenser.push(keyValue);

        if (condenser.length > 2) {
            output.value = calculator(condenser);
            firstValue = output.value;
            condenser = [];
            condenser.push(firstValue);
            condenser.push(keyValue);
        }
    }
}

document.addEventListener("DOMContentLoaded", addZero);

for (const button of buttons) {
    button.addEventListener("click", () => {
        let keyValue = button.value;
        main(keyValue);
    })
}

document.addEventListener("keyup", (event) => {

    let keyPress = event.key;
    let allowedKeys = [];

    for (let i = 0; i < buttons.length; i++) {
        allowedKeys.push(buttons[i].value);
    }

    allowedKeys.push("Enter");

    if (allowedKeys.includes(keyPress)) {
        main(keyPress);
    }
});
