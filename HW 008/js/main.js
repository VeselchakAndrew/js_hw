const priceInput = document.getElementById("price_input");
const priceDiv = document.querySelector(".price");
const closeBtn = document.querySelectorAll(".btn");
const errorDiv = document.querySelector(".error");

function deleteElem() {
    if (document.getElementById("data")) {
        const data = document.getElementById("data");
        data.remove();
        priceDiv.classList.remove("show_block");
    }
    else {
        errorDiv.classList.remove("show_block");
    }
    priceInput.value = "";
    priceInput.classList.remove("data_highlight", "border_error", "data_error");
}

priceInput.addEventListener("focus", (event) => {
    event.target.classList.add("border_highlight");
    if (priceInput.value) {
        deleteElem()
    }
});


priceInput.addEventListener("blur", (event) => {
    if (priceInput.value > 0) {
        if (!document.getElementById("data")) {
            priceDiv.insertAdjacentHTML("afterbegin", `<span id="data">Текущая цена: ${priceInput.value}</span>`);
        }
        priceDiv.classList.add("show_block");
        priceInput.classList.add("data_highlight");

    }
    else if (priceInput.value === "") {

    }
    else {
        if (!document.getElementById("error")) {
            errorDiv.insertAdjacentHTML("afterbegin", `<span id="error">Please enter correct price</span>`);
        }
        event.target.classList.add("border_error");
        errorDiv.classList.add("show_block");
        priceInput.classList.add("data_error");

    }
    event.target.classList.remove("border_highlight");
    console.log(priceInput.value)
});

for (const btn of closeBtn) {
    btn.addEventListener("click", () => {
        deleteElem();
    });
}

