const tabsContainer = document.querySelector(".tabs-content");
const buttonsContainer = document.querySelector(".tabs");
const buttons = buttonsContainer.querySelectorAll("li");
const tabs = tabsContainer.querySelectorAll("li");


function addDataAttr() {
    const activeButton = document.querySelector(".tabs > li:first-child");
    const activeTab = document.querySelector(".tabs-content > li:first-child");
    activeButton.classList.add("button--active");
    activeTab.classList.add("tabs--active");

    for (let i = 0; i < buttons.length; i++) {
        if (!buttons[i].classList.contains("tabs-title")) {
            buttons[i].classList.add("tabs-title")
        }
    }

    for (let i = 0; i < tabs.length; i++) {
        if (!tabs[i].classList.contains("tab")) {
            tabs[i].classList.add("tab")
        }
    }



    for (let i = 0; i < buttons.length; i++) {
        buttons[i].setAttribute("data-for-tab", `${i}`)
    }

    for (let i = 0; i < tabs.length; i++) {
        tabs[i].setAttribute("data-tab", `${i}`)
    }
}

function jsTabs(button) {
    const tabNumber = button.dataset.forTab;
    const tabToActive = tabsContainer.querySelector(`.tab[data-tab="${tabNumber}"]`);

    buttons.forEach(button => {
        button.classList.remove("button--active")
    });

    tabs.forEach(tab => {
        tab.classList.remove("tabs--active")
    });

    button.classList.add("button--active");
    tabToActive.classList.add("tabs--active");
}


buttons.forEach(button => {
    button.addEventListener("click", () => {
        jsTabs(button);

    })
});

document.addEventListener("DOMContentLoaded", () => {
    addDataAttr();

});
