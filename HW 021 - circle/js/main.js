const buttonDrawCircle = document.querySelector(".draw_circle");
const mainDiv = document.querySelector(".main");
const inputLabel = document.querySelector(".inputLabel");
const wrapper = document.querySelector(".wrapper");
const inputElem = `<label>Введите диаметр <input type="text" id="diameterInput"></label>`;

function createForm() {
    if (!document.getElementById("diameterInput")) {
        buttonDrawCircle.insertAdjacentHTML("afterend", inputElem);
    }
}

function getDiameter() {
    return document.getElementById("diameterInput").value
}

function deleteHtml() {
    mainDiv.remove();
}

function setRandomColor() {
    return Math.floor(Math.random() * 255)
}

function createCircle(circleAmount, diameter) {
    const roundDivMargin = 10;
    const roundInRow = 10;
    let circleHtml = document.createElement("div");
    circleHtml.setAttribute("style", `width: ${(diameter * roundInRow) + ((roundDivMargin * 2) * (roundInRow + 1))}px`);

    for (let i = 0; i < circleAmount; i++) {
        let span = document.createElement("span");
        span.classList.add("round_div");
        span.setAttribute("style", `background-color: rgb(${setRandomColor()}, ${setRandomColor()}, ${setRandomColor()}); width: ${diameter}px; height: ${diameter}px`);
        circleHtml.append(span)
    }
    return circleHtml
}

let clickCount = 0;

buttonDrawCircle.addEventListener("click", () => {
    if (clickCount === 0) {
        createForm();
    }
    else {
        let diameter = getDiameter();
        deleteHtml();
        wrapper.insertAdjacentElement("afterbegin", createCircle(100, diameter));
    }

    clickCount++;

});


document.body.addEventListener("click", (event) => {
    if (event.target.className === "round_div") {
        event.target.remove()
    }
});