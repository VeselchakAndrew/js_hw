const images = document.querySelectorAll(".image-to-show");
const next = document.querySelector(".right");
const previous = document.querySelector(".left");
const stopBtn = document.querySelector(".stop");
const startBtn = document.querySelector(".start");

let activeImg = 0;
let timeMs = 10000;
let timerId;

function startSlideShow() {
    slideShow("next");
    timerId = setTimeout(startSlideShow, timeMs);
}

function stopSlideShow() {
    clearTimeout(timerId);
}

next.addEventListener("click", () => {
    slideShow("next");
    stopSlideShow();
    timerId = setTimeout(startSlideShow, timeMs);
});

previous.addEventListener("click", () => {
    slideShow("prev");
    stopSlideShow();
    timerId = setTimeout(startSlideShow, timeMs);
});

function slideShow(direction) {

    if (direction === "next") {
        activeImg++;

        if (activeImg === images.length) {
            activeImg = 0;
        }

    }

    else {

        if (activeImg === 0) {
            activeImg = images.length - 1;
        }

        else {
            activeImg--;
        }


    }
    images.forEach(image => {
        image.classList.remove("active")
    });

    images[activeImg].classList.add("active");

}

function showButtons() {

    stopBtn.classList.remove("hidden_button");
}

document.addEventListener("DOMContentLoaded", () => {
    setTimeout(startSlideShow, timeMs);
    showButtons();

});

stopBtn.addEventListener("click", () => {
    stopSlideShow();
    startBtn.classList.remove("hidden_button");

});

startBtn.addEventListener("click", () => {
    stopSlideShow();
    setTimeout(startSlideShow, timeMs);
    startBtn.classList.add("hidden_button");
});
