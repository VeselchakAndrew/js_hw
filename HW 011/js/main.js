const buttons = document.querySelectorAll(".btn");

document.addEventListener("keyup", (event) => {

    let keyPress = event.key;
    buttons.forEach(button => {

        if (button.innerHTML === keyPress || button.innerHTML.toLowerCase() === keyPress) {
            buttons.forEach(button => {
                button.classList.remove("btn_highlight")
            });

            button.classList.add("btn_highlight")
        }
    })

});