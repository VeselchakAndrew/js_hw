let userName;
let userAge;

do {
    userName = prompt("Please enter your name", "User")
} while (userName === null || userName.trim() === "")


do {
    userAge = prompt(`Please enter age, ${userName}`, userAge)
} while (userAge === null || isNaN(Number(userAge)) || userAge.trim() === "" )


if (Number(userAge < 18)) {
    alert("You are not allowed to visit this website!")
}
else if (Number(userAge) >= 18 && Number(userAge) <= 22) {
    let userAnswer = confirm('Are you sure you want to continue?');
    if (userAnswer) {
        alert(`Welcome, ${userName}.`)
    }
    else {
        alert("You are not allowed to visit this website!")
    }
}
else {
    alert(`Welcome, ${userName}.`)
}


