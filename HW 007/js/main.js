let arr = ['hello', { "Vasya": 17, "Petya": 25 }, [1, 2, 3, [31, 32, 33, 34], 4, 5, 6], 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function listToHtml(arr) {
    const liItems = arr.map(function (arrItem) {
        if (Array.isArray(arrItem)) {
            return listToHtml(arrItem)
        }

        else if (typeof arrItem === "object") {
            return listToHtml(Object.entries(arrItem))
        }

        else {
            return `<li>${arrItem}</li>`
        }
    });

    return `<ul>${liItems.join('')}</ul>`;
}

const list = document.querySelector(".list");
list.innerHTML = listToHtml(arr);


const ul = document.querySelector("ul");
setTimeout(() => ul.remove(), 10000);

const timer = document.querySelector(".timer");
const message = document.querySelector(".message");

let timerId = setInterval(() => timer.innerHTML--, 1000);
setTimeout(() => {clearInterval(timerId); message.innerHTML = "Содержимое очищено";}, 10000);
