let m;
let n;
let primeNum = '';

do {
    m = +prompt("Enter M");
    n = +prompt("Enter N");
} while (m === null || isNaN(m) || m === "" || m % 1 !== 0
&& n === null || isNaN(Number(n)) || n === "" || n % 1 !== 0);

nextIter:
    for (m; m <= n; m++) {

        for (let i = 2; i < m; i++) {
            if (m % i === 0) continue nextIter;
        }
        primeNum += m + " ";

    }
console.log(primeNum);