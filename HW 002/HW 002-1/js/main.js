let num;
let result = "";

do {
    num = prompt("Enter number")
} while (num === null || isNaN(Number(num)) || num.trim() === "" || (Number(num)) % 1 !== 0);

// 0 деленное на любое число - будет 0, поэтому оно будет кратное любому числу. И если искать числа в диапазоне от 0,
// то кратное число будет всегда.
num = Number(num);
for (let i = 1; i <= num; i++) {
    if (i % 5 === 0) {
        result += i + " "
    }
}
console.log(result ? result : "Sorry, no numbers");