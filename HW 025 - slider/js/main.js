const images = document.querySelectorAll(".image-to-show");
const next = document.querySelector(".right");
const previous = document.querySelector(".left");
const stopBtn = document.querySelector(".stop");
const startBtn = document.querySelector(".start");

let activeImg = 0;

function startSlideShow() {
    slideShow("next");
   }



next.addEventListener("click", () => {
    slideShow("next");

});

previous.addEventListener("click", () => {
    slideShow("prev");

});

function slideShow(direction) {

    if (direction === "next") {
        activeImg++;

        if (activeImg === images.length) {
            activeImg = 0;
        }

    }

    else {

        if (activeImg === 0) {
            activeImg = images.length - 1;
        }

        else {
            activeImg--;
        }


    }
    images.forEach(image => {
        image.classList.remove("active")
    });

    images[activeImg].classList.add("active");

}
